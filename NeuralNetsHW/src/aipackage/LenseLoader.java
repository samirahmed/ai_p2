package aipackage;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class LenseLoader extends DataProcessor
{
																		
    public double[][] m_inputvs;
    public double[][] m_outputvs;

    private List<CreditData> m_datas;

    class LenseData
    {
        public LenseData(double[] inputs, double[] outputs)
        {
            m_inputs = inputs;
            m_outputs = outputs;
        }

        public double[] m_inputs;
        public double[] m_outputs;
    }

    double cvtDouble(String [] candidates, String name)
    {
        for (int i = 0; i < candidates.length; ++i)
        {
            if (candidates[i].equals(name))
            {
                return i;
            }
        }
        return candidates.length;
    }

    public LenseLoader(String aFileName) throws FileNotFoundException
    {
        m_datas = new ArrayList<CreditData>();
        Scanner s = null;

        FileReader f = new FileReader(aFileName);

        try
        {
            s = new Scanner(new BufferedReader(f));
            s.useLocale(Locale.US);

            while (s.hasNextLine())
            {
                CreditData data = processLine(s.nextLine());
                m_datas.add(data);
            }
        }
        finally
        {
            s.close();
        }

        int i = 0;
        m_inputvs = new double[m_datas.size()][];
        m_outputvs = new double[m_datas.size()][];
        for (CreditData data: m_datas)
        {
            m_inputvs[i] = data.m_inputs;
            m_outputvs[i] = data.m_outputs;
            ++i;
        }
    }

    private double nextDouble(Scanner s)
    {
        if (s.hasNextDouble())
        {
            return s.nextDouble();
        }
        else
        {
            s.next();
            return 0.0;
        }
    }

    public CreditData processLine(String line)
    {
        Scanner scanner = new Scanner(line);
        scanner.useDelimiter(",");

        double[] inputs = new double[9];
        double[] outputs = new double[3];
        double age = nextDouble(scanner);
        if (age == 1)
        {
        	inputs[0] = 1.0;
        }
        else if(age ==2)
        {
        	inputs[1] = 1.0;
        }
        else if(age == 3)
        {
        	inputs[2] = 1.0;
        }
        
        double pres = nextDouble(scanner);
        if(pres == 1)
        {
        	inputs[3] = 1.0;
        }
        else if(pres == 2)
        {
        	inputs[4] = 1.0;
        }
        double ast = nextDouble(scanner);
        if(ast == 1)
        {
        	inputs[5] = 1.0;
        }
        else if(ast == 2)
        {
        	inputs[6] = 1.0;
        }
        double tear = nextDouble(scanner);
        if(tear == 1)
        {
        	inputs[7] = 1.0;
        }
        else if(tear == 2)
        {
        	inputs[8] = 1.0;
        }

        double output = nextDouble(scanner);
        if(output == 1)
        {
        	outputs[0] = 1.0;
        }
        else if(output == 2)
        {
        	outputs[1] = 1.0;
        }
        else if(output == 3)
        {
        	outputs[2] = 1.0;
        }

        return new CreditData(inputs, outputs);
    }
}
